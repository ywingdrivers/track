#include <FS.h>
#include <Memory.h>

String latArr[] = {};

void Memory::append(float lat, float lon, float gpsTime){
  gpsTime = gpsTime/100;

  String lineAppend = String(lat, 6) + "," + String(lon, 6) + "," + (String) ((int) gpsTime);

  File f = SPIFFS.open("/data.txt", "a");

  if (!f) {
    Serial.println("File open for APPENDING failure");
  } else {
    if (lat == 0.0) {
      Serial.println("GPS IS DEAD");
    }
    else {
      f.print(lat); f.print(","); f.print(lon); f.print(","); f.print(gpsTime); f.println();
    }
  }
}

float * Memory::getLatArr(){
  File f = SPIFFS.open("/data.txt", "r");
  String temp = "";
  String garbage = "";
  float latArr[] = {};
  int count = 0;
  // String pos = "";

  latArr[0] = 123.455678;
  latArr[1] = 123.455678;
  latArr[2] = 123.455678;
  latArr[3] = 123.455678;
  Serial.println("added some nubmers");

  if (!f) {
    Serial.println("File open for READING failure");
  } else {
    Serial.println("reading open");
    while(f.position() < f.size()){

      // garbage = f.readStringUntil(',');
      // garbage = f.readStringUntil(',');
      // temp = f.readStringUntil('\n');
      // temp.trim();
      // latArr[count] = temp;
      // count++;

      garbage = f.readStringUntil(',');
      garbage = f.readStringUntil(',');
      temp = f.readStringUntil('\n');
      //Serial.println(temp);
      latArr[count] = temp.toFloat();
      count++;
      delay(100);
    }
  }
  f.close();
  return latArr;
}

String* Memory::getLonArr(){
  File f = SPIFFS.open("/data.txt", "r");
  String temp = "";
  String garbage = "";
  String* lonArr = {};
  int count = 0;

  if (!f) {
    Serial.println("File open for READING failure");
  } else {
    while(f.position() < f.size()){
      garbage = f.readStringUntil(',');
      temp = f.readStringUntil(',');
      temp.trim();
      lonArr[count] = temp;
      count++;
      garbage = f.readStringUntil('\n');
    }
  }
  f.close();
  return lonArr;
}

String* Memory::getTimeArr(){
  File f = SPIFFS.open("/data.txt", "r");
  String temp = "";
  String garbage = "";
  String* latArr = {};
  int count = 0;

  if (!f) {
    Serial.println("File open for READING failure");
  } else {
    while(f.position() < f.size()){
      garbage = f.readStringUntil(',');
      temp.trim();
      latArr[count] = temp;
      count++;
      // Garbage
      garbage = f.readStringUntil('\n');
    }
  }
  f.close();

  return latArr;
}

// Used to allow user to view file size and close file at the same time
void Memory::close(){
  File f = SPIFFS.open("/data.txt", "a");

  if (!f) {
    Serial.println("File open for CLOSING failure");
  } else {
    Serial.print("File size: "); Serial.println(f.size());
    f.close();
  }
}

// Mostly used for testing
void Memory::read(){
  File f = SPIFFS.open("/data.txt", "r");
  String s = "";

  if (!f) {
    Serial.println("File open for READING failure");
  } else {
    while(f.position() < f.size()){
      s = f.readStringUntil('\n');
      s.trim();
      Serial.println(s);
      delay(500);
    }
  }
  f.close();
}

void Memory::clear(){
  File f = SPIFFS.open("/data.txt", "w");

  if (!f) {
    Serial.println("File open for CLEARING failure");
  } else {
    Serial.println("File clear success");
  }
  f.close();
}

void Memory::init(){
  SPIFFS.begin();

  Serial.println("SPIFFS init successful.");
}
