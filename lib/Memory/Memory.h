#ifndef Memory_H
#define Memory_H

#include <Arduino.h>
#include <FS.h>

class Memory
{
  public:
		void append(float lat, float lon,float gpsTime);
		void close();
    void read();
    void clear();
    float * getLatArr();
    String* getLonArr();
    String* getTimeArr();
		void init();
  private:
};
#endif
