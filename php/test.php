<?php
include 'config.php';
include 'utilities.php';

error_reporting(E_ALL);
ini_set('display_errors', '1');

// Get varaibles from POST request, use sanitization script.
$data = json_decode(file_get_contents('php://input'), true);
print_r($data['times']);

// foreach ($data->times as $value) {
//     echo $value;
//     // echo $data->lat;
//     // echo $data->long;
//
//   }
//   //$stmt->execute();
// }


try {
    $conn = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO latlong (time, lat, lng) VALUES (:tstamp, :lat, :lng)");
    $stmt->bindParam(':tstamp', $timestamp);
    $stmt->bindParam(':lat', $lat);
    $stmt->bindParam(':lng', $lng);

    for($i = 0; $i < count($data['times']); ++$i) {
        $timestamp = $data['times'][$i];
        $lng = $data['long'][$i];
        $lat = $data['lat'][$i];
        $stmt->execute();
    }

    echo "New records created successfully";
    }
catch(PDOException $e)
    {
    echo "Error: " . $e->getMessage();
    }
$conn = null;


 ?>
