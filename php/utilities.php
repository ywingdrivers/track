<?php
// utilities.php
// ----------
// 1-12-2018 WJC

// From W3Schools https://www.w3schools.com/php/php_form_validation.asp
// NOT MEANT to be an end-all sanitization function, more of a precaution.
// Use prepared statements.

error_reporting(E_ALL);
ini_set('display_errors', '1');

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


?>
