function initMap() {
  var t = {
    lat: titanicLat,
    lng: titanicLong
  };
  map = new google.maps.Map(document.getElementById("divBR"), {
    zoom: 9,
    center: t
  });
  new google.maps.Marker({
    position: t,
    map: map
  });
  heatmap = new google.maps.visualization.HeatmapLayer({
    data: processGPSData(),
    map: map
  })
}

function processGPSData() {
  myPoints = [];
  for (var t = 0; t < GPSData[0].length / 2; ++t) myPoints.push(new google.maps.LatLng(GPSData[0][t], GPSData[1][t]));
  return console.log(myPoints), myPoints
}
API_KEY = "AIzaSyCk-Qr3LlPgeWbOsceNpKBq6DE1pdA_AhU";
var map;
for (width = window.innerWidth / 2, height = window.innerHeight / 2, margin = 80, svgArray = [], gArray = [], svgName = ["Temerature (Celsius)", "Accelerometer", "Speed (mph)"], svgColors = ["red", "blue", "yellow", "green"], svgArray.push(d3.select("#divTL").append("svg")), svgArray.push(d3.select("#divTR").append("svg")), svgArray.push(d3.select("#divBL").append("svg")), i = 0; i < 3; i++) gArray.push(svgArray[i].append("g").attr("transform", "translate(" + margin / 2 + "," + margin / 2 + ")"));
var xTime = d3.scaleLinear().rangeRound([0, width - 2 * margin]).domain(d3.extent(timeArray, function(t) {
    return t
  })),
  yTemp = d3.scaleLinear().rangeRound([height - margin, 0]).domain(d3.extent(tempData, function(t) {
    return t
  })),
  yAccl = d3.scaleLinear().rangeRound([height - margin, 0]).domain(d3.extent(accelData[0].concat(accelData[1]).concat(accelData[2]), function(t) {
    return t
  })),
  ySpeed = d3.scaleLinear().rangeRound([height - margin, 0]).domain(d3.extent(speedData, function(t) {
    return t
  }));
lineTemp = d3.line().x(function(t, e) {
  return xTime(timeArray[e])
}).y(function(t) {
  return yTemp(t)
}), lineAccel = d3.line().x(function(t, e) {
  return xTime(timeArray[e])
}).y(function(t) {
  return yAccl(t)
}), lineSpeed = d3.line().x(function(t, e) {
  return xTime(timeArray[e])
}).y(function(t) {
  return ySpeed(t)
});
for (var i = 0; i < 3; ++i) gArray[i].append("g").attr("transform", "translate(0," + (height - margin) + ")").call(d3.axisBottom(xTime)).select(".domain").remove();
gArray[0].append("g").call(d3.axisLeft(yTemp)).append("text").attr("fill", svgColors[3]).attr("transform", "rotate(-90)").attr("y", 6).attr("dy", "0.71em").attr("text-anchor", "end").attr("font-size", 15).text(svgName[0]), gArray[0].append("path").datum(tempData).attr("fill", "none").attr("stroke", svgColors[3]).attr("stroke-linejoin", "round").attr("stroke-linecap", "round").attr("stroke-width", 2.5).attr("d", lineTemp), gArray[1].append("g").call(d3.axisLeft(yAccl)).append("text").attr("fill", svgColors[1]).attr("transform", "rotate(-90)").attr("y", 6).attr("dy", "0.71em").attr("text-anchor", "end").attr("font-size", 15).text(svgName[1]);
for (var i = 0; i < 3; ++i) gArray[1].append("path").datum(accelData[i]).attr("fill", "none").attr("stroke", svgColors[i]).attr("stroke-linejoin", "round").attr("stroke-linecap", "round").attr("stroke-width", 2.5).attr("d", lineAccel);
gArray[2].append("g").call(d3.axisLeft(ySpeed)).append("text").attr("fill", svgColors[3]).attr("transform", "rotate(-90)").attr("y", 6).attr("dy", "0.71em").attr("text-anchor", "end").attr("font-size", 15).text(svgName[3]), gArray[2].append("path").datum(speedData).attr("fill", "none").attr("stroke", svgColors[2]).attr("stroke-linejoin", "round").attr("stroke-linecap", "round").attr("stroke-width", 2.5).attr("d", lineSpeed), document.getElementById("tempText").innerHTML = tempData[tempData.length - 1] + "&nbsp;<small>&deg;C</small>", htmlList = "<h3>Last 10 GPS readings</h3><ul>", GPSData[0].reverse(), GPSData[1].reverse();
for (var i = 0; i < 10; ++i) htmlList += "<li><b>Lat:</b> " + GPSData[0][i].toFixed(3) + "  <b>Lng:</b> " + GPSData[1][i].toFixed(3) + "</li>";
document.getElementById("divLastGPS").innerHTML = htmlList;
