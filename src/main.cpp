#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include "FS.h"
#include "GPS.h"
// #include <WiFiManager.h>
const char* ssid = "Patrick's wifi";
const char* password = "morgan2235";
const int dataSize = 10;
float data[dataSize][3];
int end = 0;
int count = 0;
String lat;
String lon;
String tim;
int i = 0;

GPS myGPS;
File a;
// WiFiManager internet;

void setup() {
  Serial.begin(9600);
  Serial.setDebugOutput(true);

  myGPS.init();
  SPIFFS.begin();

  //
  // Serial.println();
  // Serial.print("Connecting");
  // while (WiFi.status() != WL_CONNECTED)
  // {
  //   delay(500);
  //   Serial.print(".");
  // }
  //
  // Serial.println("success!");
  // Serial.print("IP Address is: ");
  // Serial.println(WiFi.localIP());
  //
  // delay(500000);
  // Serial.println("After Delay");

  a = SPIFFS.open("/data.txt", "w");

  if (!a) {
    Serial.println("File open for CLEARING failure");
  } else {
    Serial.println("FILE CLEARED IN BEGINNING");
  }
  a.close();

  myGPS.smartDelay(1000);
  delay(4000);
}

void loop() {
  float latt = myGPS.getLat();
  float lonn = myGPS.getLon();
  int timm = myGPS.getTime();

  Serial.print("Lat: "); Serial.println(latt, 6);
  Serial.print("Lon: "); Serial.println(lonn, 6);
  Serial.print("Timm: "); Serial.println(timm);

  // data[0][0] = latt;
  // data[0][2] = latt;
  a = SPIFFS.open("/data.txt", "a");
  if (!a) {
    Serial.println("File open for APPENDING failure");
  } else {
      a.print(latt,7); a.print(","); a.print(lonn,7); a.print(","); a.print(timm); a.println();
  }
  a.close();
  Serial.println("Built Data In File");
  myGPS.smartDelay(500);

  if(count == 10){
    latt = 42.0;
    lonn = 42.0;
  }

  //WiFi Loop
  if(latt == 42.0 && lonn == 42.0) {
    a = SPIFFS.open("/data.txt", "r");
    if (!a) {
      Serial.println("File open for READING failure");
    } else {
      while(a.position() < a.size()){

        lat = a.readStringUntil(',');
        lon = a.readStringUntil(',');
        tim = a.readStringUntil('\n');


        lat.trim();
        lon.trim();
        tim.trim();


        // Serial.print("Lat: "); Serial.println(lat.toFloat(),6);
        // Serial.print("Lon: "); Serial.println(lon.toFloat(),6);
        // Serial.print("Time: "); Serial.println(tim.toInt());

        data[i][0] = lat.toFloat();
        data[i][1] = lon.toFloat();
        data[i][2] = tim.toFloat();
        i++;
      }
    }
    a.close();

    SPIFFS.open("/data.txt", "w");
    Serial.println("File Cleared");
    delay(1000);

    SPIFFS.remove("/data.txt");
    Serial.println("File Removed");
    delay(1000);

    if(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, password);
      WiFi.mode(WIFI_STA);

      Serial.println();
      Serial.print("Connecting");

      while (WiFi.status() != WL_CONNECTED){
        delay(500);
        Serial.print(".");
      }
    }

    DynamicJsonBuffer JSONbuffer;
    JsonObject& JSONencoder = JSONbuffer.createObject();

    JsonArray& lats = JSONencoder.createNestedArray("lat");
    JsonArray& longs = JSONencoder.createNestedArray("long");
    JsonArray& times = JSONencoder.createNestedArray("times");

    for(int j = 0; j < dataSize; j++){
      lats.add(data[j][0]);
      longs.add(data[j][1]);
      times.add(data[j][2]);
    }
    data[0][0]={0};

    // internet.autoConnect("Please Work");

    // lats.add(data[0][0]);
    // longs.add(data[0][1]);
    // times.add(data[0][2]);

    Serial.println("Built arrays");
    myGPS.smartDelay(500);

    char messageJSON[1300];
    //lats.printTo(Serial);
    JSONencoder.prettyPrintTo(messageJSON, sizeof(messageJSON));
    //Serial.println(messageJSON);

    HTTPClient http;
    http.begin("http://buttedigital.com/william/test.php"); //Specify request destination
    http.addHeader("Content-Type", "application/json"); //Specify content-type header

    int httpCode = http.POST(messageJSON); //Send the request
    String payload = http.getString(); //Get the response payload
    Serial.print("HTTP CODE: "); Serial.println(httpCode); //Print HTTP return code
    Serial.print("Payload: "); Serial.println(payload); //Print request response payload
    http.end(); //Close connection

    count = 0;
    // ESP.wdtFeed();
    // Serial.println("WDT FED");
    Serial.println("Finished");
    myGPS.smartDelay(1000);
    JSONbuffer.clear();
  }
  myGPS.smartDelay(500);
  count++;
}
